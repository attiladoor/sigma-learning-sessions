# Machine learning basics

Machine Learning, as the name suggests, provides machines with the ability to learn autonomously based on experiences, observations and analysing patterns within a given data set without explicitly programming. When we write a program or a code for some specific purpose, we are actually writing a definite set of instructions which the machine will follow. Whereas in machine learning, we input a data set through which the machine will learn by identifying and analysing the patterns in the data set and learn to take decisions autonomously based on its observations and learnings from the dataset. [[1]](https://www.quantinsti.com/blog/machine-learning-basics)

### Use cases
Machine learning is used various purposes, here is a brief list of some uses cases:
- Classiﬁcation
- Object detection (classification)
- Regression
- Anomaly detection

and many others

### Type of Machine learning

We can distingish 3 types of learning method:

- Supervised: Supervised learning algorithms build a mathematical model of a set of data that contains both the inputs and the desired outputs
- Unsupervised: Unsupervised learning algorithms take a set of data that contains only inputs, and find structure in the data, like grouping or clustering of data points.
- Reinforced: Reinforcement learning is an area of machine learning concerned with how software agents ought to take actions in an environment so as to maximize some notion of cumulative reward 

In this tutorial we will focus on supervised learning. [[2]](https://en.wikipedia.org/wiki/Machine_learning)


## Neural Networks

The most fundamental unit of a deep neural network is called an artificial neuron, which takes an input, processes it, passed it through an activation function like the Sigmoid, return the activated output.

The perceptron consists of 4 parts:
- Input values or One input layer
- Weights and Bias
- Net sum
- Activation Function


![alt text](session3-ML-basics/pics/perceptron.png)

[[source]](https://towardsdatascience.com/what-the-hell-is-perceptron-626217814f53)

By perceptrons, we can build two types of neural networks:
- Feedforward neural network (we will focus on this one)
- Recurrent Neural Networks 

![alt text](session3-ML-basics/pics/feed_forward_nn.png)

[[source]](https://en.wikipedia.org/wiki/Feedforward_neural_network)

## Training

After we specified our neural network, we should set the weights between neurons. These weights are adjusted over an optimization process. The optimization must be performed over a parameter, we should define what is supposed to be optimized. In ML they call it [loss](https://developers.google.com/machine-learning/crash-course/descending-into-ml/training-and-loss) that is usually called [cross-entropy](https://ml-cheatsheet.readthedocs.io/en/latest/loss_functions.html) in classification. 

Imagine the optimization as finding a minimum of a multivariable function. In 2D case, it would find the minimum of a surface but of course in ML, we have way more variables. Let's go back to the 2D example and see how it works. When we are one point of the surface, we should look at derivative to each direction and see where is it decreasing and once we found it, we should make a step and check it again. In ML this step is called [learning rate](https://towardsdatascience.com/understanding-learning-rates-and-how-it-improves-performance-in-deep-learning-d0d4059c1c10). Finding the optimal learning rate is import since if the rate is small, the learning process would be too slow meanwhile if the learning rate is too high, then we would jump over the minimum and we may miss it. 

Keep in mind at finding the minimum that a function may have multiple minima and there is no guarantee we would find the global minimum. This whole optimization is called [gradient descent](https://en.wikipedia.org/wiki/Gradient_descent) optimization.

But how does this value adjustment work on the neural network? The applied algorithm is called [backpropagation](https://en.wikipedia.org/wiki/Backpropagation).

![alt text](session3-ML-basics/pics/learning_rate.png)

## Performance measurement

When we perform the training of our neural network, we exercise the training data on the model and see the response (that will define the loss) but in the same time we measure this accuracy and if everything goes well, it should be increasing at each iteration. 

But how do we know how good is the model? We should measure the final accuracy but for all this, we need a data set that is independent of the training data. It is like a math course where you got a lot of training exercise but on the final exam, you should get something different, otherwise, we don't know the student memorized all the answers or really understood the subject. 

That's why we measure training and test accuracy with an independent data set. 

## Modelling

In the previous sections we learned about how neural networks are built up and how to train them but we haven't talked about how to choose a good model for our purposes. In order to understand it, let's take an example from statistics where we have a set of datapoint (that can be noisy) and we would like to align a curve by regression. If we take the most simple case and use linear regression, that can perform well but only when the data set also correlates to a linear function. If it is a noisy 2-degree polynomial function, then our model is not complex enough for the purpose, and it is called [underfitting](https://medium.com/greyatom/what-is-underfitting-and-overfitting-in-machine-learning-and-how-to-deal-with-it-6803a989c76) and then, either the training and test accuracy are low. Meanwhile, if we choose a too complex model, so a high degree polynomial function, then it would perfectly fit on the training data, but would be very poor on generalization. It is called [overfitting](ttps://medium.com/greyatom/what-is-underfitting-and-overfitting-in-machine-learning-and-how-to-deal-with-it-6803a989c76) and it would result a high training accuracy but poor test accuracy. 

![alt text](session3-ML-basics/pics/Overfitting.png)

Therefore, in neural networks, important to find the sufficient size and complexity of the model and make it "lagom". 

### Regularization

Previously we defined loss as the inaccuracy of model and we wanted to minimalize that.
![alt text](session3-ML-basics/pics/RSS.png)

In regularization, we assign a cost for the "number" of parameters as well. It is utilized over introducing a "cost" function of weights, that is most commonly L2 (RMS) or L1 (average) of weights. By this method, we can minimalize the number of parameters and avoid overfitting. [[source]](https://towardsdatascience.com/regularization-in-machine-learning-76441ddcf99a)

![alt text](session3-ML-basics/pics/RSS_w_regularization.png)

## Summary

We learned some basics of how do neural network work and how does the training process work in nutshell, but keep in mind it is a very broad topic and we touch only the surface of it. If you have time, i would recommend you reading [the deep learning book](https://www.deeplearningbook.org/)