# Multi-threading in C++ #

Before C++11, there was no standard, platform independent libarary for multi-threading. You had to use some OS specific library (e.g.: pthread on Linux), that worked actually good enough but now, this feature is built into the standard library.

Run the example codes in: https://www.onlinegdb.com/

## Example 1 ##

Look at the example code: [here](https://gitlab.com/attiladoor/sigma-learning-sessions/blob/master/session1-multi-threading/example1.cpp) you can see, thread object can be initilized multiple way. All the way, when we start a new thread, we must specify what should be executed in the given thread. In the example code you can see 3 different solutions, where we pass is as:
- function pointer
- function object
- lambda

The syntax is the following: 
```cpp
void foo() {
...
}

void bar(int x) {
...
}
//std::thread th(<function>, <params>);
std::thread th1(foo);
std::thread th2(bar, 2)
```

For thread sincronization, we can use [*join*](http://www.cplusplus.com/reference/thread/thread/join/) method that will return when the given thread changes its status to [*joinable*](http://www.cplusplus.com/reference/thread/thread/joinable/). It can be useful when you initilize the GUI in a thread and at some point you would like to wait for finishing the initialization. It can be accessed in any threads, so the following code is valid: 

```cpp
    thread th4([](thread* x) {
        
        x->join();
    }, &th1);
```
At calling *join*, the thread is not destucted, it will be when the object goes out of scope (but the execution can be terminated anytime). However if you don't *join* or *detach* your thread, it will call *std::terminate*, but that is not adviced. You must be sure all your threads are joined at the end. 

By [*detach*](http://www.cplusplus.com/reference/thread/thread/detach/)ing your thread, you can make the execution independent from your calling thread. 

If you run the example code, you will get something similar:

```bash
Threads 1 and 2 and 3 operating independently
Thread using function pointer as callable
Thread using function pointer as callable
Thread using function pointer as callable
Thread using function object as  callable
Thread using function object as  callable
Thread using function object as  callable
Thread using lambda expression as callable
Thread using lambda expression as callable
Thread using inline labmda expression
Thread using inline labmda expression
Thread using inline labmda expression
Thread using lambda expression as callable
```
As you can see, the output lines are not in call-order, because the threads are not synronized but they are racing for resources.(see later)

## Example 2 ##

Each thread is accessible inside the thread by usin *std::this_thread* namespace:
```cpp
#include <thread>
#include <iostream>
#include <vector>

void hello(){
    std::cout << "Hello from thread " << std::this_thread::get_id() << std::endl;
}

int main(){
    std::vector<std::thread> threads;

    for(int i = 0; i < 5; ++i){
        threads.push_back(std::thread(hello));
    }

    for(auto& thread : threads){
        thread.join();
    }

    return 0;
}
```
```bash
Hello from thread 140276650997504
Hello from thread 140276667782912
Hello from thread 140276659390208
Hello from thread 140276642604800
Hello from thread 140276676175616
```

## Example 3 ##

In the 1st example, we could see that, threads are not syncornized and they can race for resources. Now let's take a look at [example3](https://gitlab.com/attiladoor/sigma-learning-sessions/blob/master/session1-multi-threading/example3.cpp), where the threads share an object. Let's run that code few times. 
```bash
4547
4412
4728
```
(if the results are flawless, then probably the thread creation is too slow and one thread terminates before the other one is created )

Because the increasing is executed in 3 steps:
- reading out value
- incrementing
- writing back
and meanwhile this process they can easily intervent eachothers operation. 

For tackling this issue, we can use a kind of lock that allows only 1 thread to do operations at once. It is called semaphore but in CPP we can use one of its sub-type, called mutex. 

Let's improve our by replacing *Counter* struct by:
```cpp
#include <mutex>

struct Counter {
    std::mutex mutex;
    int value;

    Counter() : value(0) {}

    void increment(){
        mutex.lock();
        ++value;
        mutex.unlock();
    }
};
```
The *lock* function first tests the variable and if it hasn't been locked, then it would do it, but if it is locked, then it is waiting. So all the threads will be waiting for the mutex to be set unlocked. It looks very convinient and smart but you must be careful and always unlock the mutex, further more at multi-resources it can easily lead a *dead-lock* 

## Additional features

### Lock Guard
Optionally, you can use [lock guard](https://en.cppreference.com/w/cpp/thread/lock_guard) that takes the ownership of the mutex at creating and lock it then later unlock it at destruction. 

### Timed Mutex
Sometimes you don't want wait forever for the resource in your thread, then you can use [*timed_mutes*](https://en.cppreference.com/w/cpp/thread/timed_mutex) or [*recursive_timed_mutex*](https://en.cppreference.com/w/cpp/thread/recursive_timed_mutex). They can be used a regular mutex, or you can use *try_lock_for* or *try_lock_until*. They both return if the conditions are not valid anymore. 

### Call once

Sometimes you want to call a function only once and instead of using some static or global flag with an if condition, you can use [*call_once*](https://en.cppreference.com/w/cpp/thread/once_flag)

```cpp
std::once_flag flag;

void do_something(){
    std::call_once(flag, [](){std::cout << "Called once" << std::endl;});

    std::cout << "Called each time" << std::endl;
}

int main(){
    std::thread t1(do_something);
    std::thread t2(do_something);
    std::thread t3(do_something);
    std::thread t4(do_something);

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    return 0;
}
```

```bash
Called once
Called each time
Called each time
Called each time
Called each time
```

## Example 4 - Condition variables

Condition Variable is a kind of Event used for signaling between two or more threads. One or more thread can wait on it to get signaled, while an another thread can signal this.

- Thread 1 calls the wait on condition variable, which internally acquires the mutex and check if required condition is met or not.
- If not then it releases the lock and waits for Condition Variable to get signaled ( thread gets blocked). Condition Variable’s wait() function provides both these operations in atomic manner.
- Another Thread i.e. like Thread 2 signals the Condition Variable when condition is met
- Once Conditional Variable get signaled the the Thread 1 which was waiting for it resumes. Then it acquires the mutex lock again and checks if the condition associated with Condition Variable is actually met or if it is superiors call. If more than one thread was waiting then notify_one will unblock only one thread.
-If it was a superiors call then it again calls the wait() function.

In [example4](https://gitlab.com/attiladoor/sigma-learning-sessions/blob/master/session1-multi-threading/example4.cpp), in the example, in *waitingForWork* is waiting for *setDataReady* to set *dataReady* variable to true and make the condition true.

The output of the example code:
```bash
Waiting 
Data prepared
Running 
```

## Atomic types

Do you remember the counter class from Example-3? 
```cpp
struct Counter {
    int value;

    Counter() : value(0){}

    void increment(){
        ++value;
    }
};
```

we saw, using standard int is not thread safe and it can be tackled by using mutex, that is a nice but a bit spamming and computationally not really efficient method. C++ support other, thread-safe types as [atomic](https://en.cppreference.com/w/cpp/atomic/atomic), It is a template type that can be used for any types you want but the thread-safe method is implemented internally, that means you cannot be sure if a lock-free (fast) method or not, would be used. 

Let's fix our counter class from the previous example:
```cpp
#include <atomic>

struct Counter {
    std::atomic<int> value;

    Counter() : value(0){}

    void increment(){
        ++value;
    }
};
```
## Future - Promise
Futures and promises are two sides of the same coin, so to speak. A promise allows us to return state from a thread. Whereas a future is for reading that returned state.

In other words, let’s think of it in this, fairly simple way – we create a promise that really says that at some time there will be a state change or return value from thread. The promise can be used to get a std::future, which is the object that gives us this return result.

So we’ve got the equivalent of a simple producer/consumer pattern [[source](http://putridparrot.com/blog/threads-promises-futures-async-c/)]

There are some really good descriptions available in this topic, for example: [[link](https://thispointer.com/c11-multithreading-part-8-stdfuture-stdpromise-and-returning-values-from-thread/)]

## Exercise
Let's make a little exercise when where we have the follwing resources:
- *PrinterResource* singleton class, that has 1 method, called *print* with 1 string argument. It does nothing else but print out the received string to the standard output
- *DataSource* singleton class, that has 1 method, called *getData*. It should generate a random number between x = 0-2000, and it would wait for *x* milliseconds and return with *x*
- *dataHandler* function, that requests data from *DataSource* and print it out over *PrinterResource* class along with its threadId, preferably in a *for* loop for 10 times

You should have at least 3 threads with *dataHandler* function, that are joined at the end.

The main point would be exercising std::thread features, so please pay attention where could you use those. (future-promise is extra point)

### sources:

https://www.geeksforgeeks.org/multithreading-in-cpp/

https://baptiste-wicht.com/posts/2012/03/cp11-concurrency-tutorial-part-2-protect-shared-data.html

http://www.cplusplus.com/reference/thread/thread/

https://thispointer.com/c11-multithreading-part-8-stdfuture-stdpromise-and-returning-values-from-thread/

