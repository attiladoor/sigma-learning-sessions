#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <vector>
#include <mutex>

std::mutex printMutex;

class PrinterResource {
    public:
    void print(std::string input) {
        std::lock_guard<std::mutex> lock(printMutex);
        std::cout << input << std::endl;
    }
};

class DataSource {
  public:
  int getData() {
      int randomNumber = rand() % 2000;
      std::this_thread::sleep_for(std::chrono::milliseconds(randomNumber));
      return randomNumber;
  }
};

int main()
{
    std::cout << "start" << std::endl;
    PrinterResource pr;
    DataSource ds;
    
    std::vector<std::thread> threads;
    for (int i = 0; i < 10; ++i) {
        threads.push_back(std::thread([&pr, &ds](){
            
            for (int i = 0; i < 10; ++i) {
                std::ostringstream ss;
                ss << std::this_thread::get_id();
                ss << " - ";
                ss << ds.getData();
                pr.print(ss.str());    
            }
        }));
    }
    
    for(auto& thread : threads){
        thread.join();
    }

    return 0;
}
